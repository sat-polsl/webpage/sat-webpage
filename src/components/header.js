import PropTypes from "prop-types"
import React from "react"

import Navbar from "./navbar"

const Header = ({ siteTitle }) => {

  return (
    <header
      style={{
        background: `#171616`,
        margin: `0 auto`,
        maxWidth: 1920,
        position: `fixed`,
        top: 0,
        right: 0,
        left: 0,
        zIndex: 1030
        //marginBottom: `1.45rem`,
      }}
    >
      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          //padding: `0.3rem 1.0875rem`,
        }}
      >
        <Navbar />
      </div>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
