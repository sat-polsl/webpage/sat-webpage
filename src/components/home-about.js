import React from "react"

import homeAboutStyles from './home-about.module.css'

const HomeAbout = () =>  {

  return (
    <section className={homeAboutStyles.outer}>
      <section className={homeAboutStyles.inner}>
        <div style={{
          width: `250px`,
          height: `3px`,
          backgroundColor: `#FFF`,
          margin: `0 auto`,
          marginBottom: `0.3rem`,
        }}/>
      <h1>Czym się zajmujemy?</h1>
      <p>
      Test Dockera! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus a pulvinar massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur id ligula laoreet, luctus urna at, scelerisque lorem. Vestibulum eget dolor aliquet, facilisis nunc sed, aliquet risus. Nulla eget vehicula erat. Nunc at diam at velit tempor tincidunt. Mauris et ex vulputate sem vehicula aliquam sed consequat nulla.
      </p>
      </section>
    </section>
  )
}

export default HomeAbout