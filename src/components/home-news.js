import React from "react"
import { useStaticQuery, graphql, Link } from "gatsby"
import homeNewsStyles from './home-news.module.css'
import { normalizePath } from "../utils/get-url-path"
import { rhythm } from "../utils/typography"
import { trimWordpressText } from "../utils/news-utils"

const HomeNews = () =>  {
  const data = useStaticQuery(graphql`
    query{
      allWpPost(
        limit: 3
        filter: { nodeType: { in: ["Post"] } }
        sort: { fields: date, order: DESC }
      ) {
        nodes {
          uri
          title
          slug
          excerpt
          date(formatString: "YYYY-MM-DD HH:mm")
        }
      }
    }
  `)

  return (
    <section className={homeNewsStyles.outer}>
      <section className={homeNewsStyles.inner}>
        <div style={{
          width: `203px`,
          height: `3px`,
          backgroundColor: `#111`,
          marginBottom: `0.4rem`,
        }}/>
        <h1>Aktualności</h1>
        {data.allWpPost.nodes.map((node) => {
          const excerpt = trimWordpressText(node.excerpt, 30);
          return (
            <article key={node.slug}>
              <header>
                <h3 style={{
                  marginBottom: rhythm(1/4),
                }}>
                  <Link 
                    to={normalizePath(node.uri)}
                    style={{ 
                      color: `#111`,
                      boxShadow: `none`,
                      textDecoration: `none`
                    }}
                  >{node.title}</Link>
                </h3>
                <small>{node.date}</small>
              </header>
              <section>
                <p dangerouslySetInnerHTML={{
                    __html: excerpt,
                  }}
                />
              </section>
            </article>
          )
        })}
      </section>
    </section>
  )
}

export default HomeNews