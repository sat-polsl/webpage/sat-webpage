import React from "react"
import Img from "gatsby-image"
import { Link, useStaticQuery, graphql } from "gatsby"
import logoStyles from "./logo.module.css"

const Logo = () => {
  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "sat-logo-horizontal.png" }) {
        childImageSharp {
          fixed(height: 100, quality: 95) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return (
    <Link className={logoStyles.logo} to="/">
      <Img fixed={data.file.childImageSharp.fixed} alt="logo" />
    </Link>
  )
}

export default Logo