import React, { useState } from "react"
import NavbarLinks from "./navbarlinks"
import navigationStyles from  './navbar.module.css'

const Navbar = () => {
  const [navbarOpen, setNavbarOpen] = useState(false)

  return (
    <section className={navigationStyles.navigation}>
      <div className={navigationStyles.toggleHamburger}
        role="presentation"
        navbarOpen={navbarOpen}
        onClick={() => setNavbarOpen(!navbarOpen)}
      >
        {navbarOpen ? (
          <div 
            className={[navigationStyles.hamburger, 
              navigationStyles.hamburgerOpen].join(' ')
            } 
            style={{
              transform: `rotate(-45deg)`
            }}    
          />
        ) : (
          <div 
            className={[navigationStyles.hamburger, 
              navigationStyles.hamburgerClosed].join(' ')
            }
            style={{
              transform: `inherit`
            }}    
          />
        )}
      </div>
      {navbarOpen ? (
        <section 
          className={navigationStyles.navbox}
          style={{ left: `0`}}
        >
          <NavbarLinks />
        </section>
      ) : (
        <section 
          className={navigationStyles.navbox}
          style={{left: `-100%`}}  
        >
          <NavbarLinks />
        </section>
      )}
    </section>
  )
}

export default Navbar