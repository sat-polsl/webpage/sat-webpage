import React from "react"
import { Link } from "gatsby"
import navbarlinkStyles from "./navbarlinks.module.css"

const NavbarLinks = ({navbarOpen}) => {
  return (
    <>
    {!navbarOpen ? (
      <>
        <Link className={navbarlinkStyles.navitem} to="/">Home</Link>
        <Link className={navbarlinkStyles.navitem} to="/news">Aktualności</Link>
        <Link className={navbarlinkStyles.navitem} to="/about">O nas</Link>
        <Link className={navbarlinkStyles.navitem} to="/contact">Kontakt</Link>
      </>
    ) : (
      <>
        <Link to="/">Home</Link>
        <Link to="/news">Aktualności</Link>
        <Link to="/about">O nas</Link>
        <Link to="/contact">Kontakt</Link>
      </>
    )}
    </>
  )
}

export default NavbarLinks