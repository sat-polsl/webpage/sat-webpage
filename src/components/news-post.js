import React from "react"

import { normalizePath } from "../utils/get-url-path"
import { Link } from "gatsby"
import { trimWordpressText } from "../utils/news-utils"
import newsPostStyles from "./news-post.module.css"

function NewsPost({ data, full}) {
  const excerpt = trimWordpressText(data.excerpt, 30);
  console.log(full)
  return(
    <article key={data.slug}>
      <header>
        <section className={newsPostStyles.titleSection}>
          <div className={newsPostStyles.titleDateSection}>
            <div className={newsPostStyles.titleDate}>
              {data.date.split(" ")[0]}
            </div>
            <p className={newsPostStyles.titleTime}>
              {data.date.split(" ")[1]}
            </p>
          </div> 

          <h1 className={newsPostStyles.title}>
            <Link 
              to={normalizePath(data.uri)}
              className={newsPostStyles.titleLink}
            >
            {data.title}
            </Link>
          </h1>
        </section>
      </header>
      {full ? (
        <section  
          className={newsPostStyles.postContent}
          dangerouslySetInnerHTML={{ __html: data.content }} 
        />
      ) : (
        <section className={newsPostStyles.excerpt}>
          <p style={{margin: `0`}} dangerouslySetInnerHTML={{
              __html: excerpt,
            }}
          />
        </section>
      )}
    </article>
  )
}

export default NewsPost