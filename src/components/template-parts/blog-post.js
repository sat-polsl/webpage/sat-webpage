import React from "react"

import SEO from "../../components/seo"
import { Link } from "gatsby"
import Layout from "../../components/layout"
import { normalizePath } from "../../utils/get-url-path"
import { rhythm, scale } from "../../utils/typography"

function BlogPost({ data }) {
  const { nextPage, previousPage, page } = data
  const { title, content, excerpt, date} = page

  return (
    <Layout>
    <SEO 
      title={title}
      description={excerpt}
    />
    <div style={{
      background: `#e6e6e6`,
      display: `inline-block`,
      width: `100%`,
    }}>
      <section style={{
        maxWidth: `960px`,
        textAlign: `justify`,
        margin: `1rem auto`,
        padding: `0 1rem`,
      }}>
        <article>
          <header>
            <h1 style={{
              marginTop: rhythm(1),
              marginBottom: 0,
            }}>
              {title}
            </h1>
            <p 
              style={{
                ...scale(-1/5),
                display: `block`,
                marginBottom: rhythm(1),
              }}
            >
              {date}
            </p>
          </header>
          <section  dangerouslySetInnerHTML={{ __html: content }} />
        </article>
      </section>
    </div>
    </Layout>
  )
}

export default BlogPost
