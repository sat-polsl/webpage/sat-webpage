import React, {useState} from "react"
import Img from "gatsby-image"
import { useStaticQuery, graphql} from "gatsby"

import SEO from "../components/seo"
import Layout from "../components/layout"
import NewsPost from "../components/news-post"
import { scale } from "../utils/typography"
import newsStyles from "../components/news.module.css"

const title = "Aktualności";

const News = () => {
  const data = useStaticQuery(graphql`
  query {
    allWpPost(
      filter: { nodeType: { in: ["Post"] } }
      sort: { fields: date, order: DESC }
    ) {
      nodes {
        uri
        title
        slug
        excerpt
        content
        date(formatString: "DD.MM.YYYY HH:mm")
      }
    }
    file(relativePath: {eq: "news-background.png"}) {
      childImageSharp {
        fluid(maxWidth: 2546, quality: 95) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
  `)
  const [selectedNews, setSelectedNews] = useState("")

  return (
    <Layout>
    <SEO title="Aktualności"/>
    <Img fluid={data.file.childImageSharp.fluid} />
    <div className={newsStyles.newsBackground}>
      <h1 style={{
        ...scale(2),
        margin: `1rem 2rem 2rem 5vw`,
      }}>
        {title}
      </h1>
          {data.allWpPost.nodes.map((node) => {
            if (selectedNews === node.slug) {
              return(
                <div className={newsStyles.selectedNewsBackground}>
                  <NewsPost data={node} full={true} />
                  <div className={newsStyles.postUnderline}/>
                  <div 
                    className={newsStyles.toggleArrow}
                    role="presentation"
                    onClick={() => setSelectedNews("")}
                  >
                    <div className={newsStyles.arrowUp}/>
                  </div>
                </div>
              )
            } else {
              return (
                <div> 
                  <NewsPost data={node} full={false} />
                  <div className={newsStyles.postUnderline}/>
                  <div 
                    className={newsStyles.toggleArrow}
                    role="presentation"
                    onClick={() => setSelectedNews(node.slug)}
                  >
                    <div className={newsStyles.arrowDown}/>
                  </div>
                </div>
              )
            }
          })}
    </div>
    </Layout>
  )
}

export default News