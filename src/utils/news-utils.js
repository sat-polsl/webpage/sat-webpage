export const trimWordpressText = (text, words) => {
  var ret;
  ret = text.substring(3, text.length - 6);
  if (ret.split(' ').length > words) {
    ret = ret.split(' ').slice(0, words).join(' ');
    ret = "<p>"+ret+"...</p>";
    return ret;
  } else {
    return text;
  }
}