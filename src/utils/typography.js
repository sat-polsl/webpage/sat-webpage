import Typography from "typography"

const typography = new Typography({
  baseFontSize: "18px",
  baseLineHeight: 1.5,
  headerFontFamily: [
    "Avenir Next",
    "Montserrat",
    "sans-serif",
  ],
  bodyFontFamily: ["Avenir", "Montserrat", "sans-serif"],
})

export default typography
export const rhythm = typography.rhythm
export const scale = typography.scale